#!/usr/bin/python3
#
# (C) Hermann Lauer 2016-2018
# can be distributed under the GPL V3 or later
GPIO_BASE_BP    = 0x01C20000
SUNXI_GPIO_BASE = 0x01C20800
PAGE_SIZE = 0x1000     #4*1024

class mmapio(object):
  """base class to allow direct io accesses via mmap
  """
  offset = SUNXI_GPIO_BASE-GPIO_BASE_BP   #calc offset in BLOCK aligned mmap
  IOBase = GPIO_BASE_BP
  IOSize = PAGE_SIZE

  def __init__(s,needatomic=False):
    """if needatomic is true, loading the c ext for frobbing bits must succeed"""
    s.gpiommap()
    try:
     import atomicfrob
     s._frob=atomicfrob.frob
     s._write=s._writeatomic
    except KeyError as e:
      if needatomic: raise e

  def calcoffsize(s,idxoff=0):
    """calulate base addr and offset and size
"""
    rem = s.IOBase%PAGE_SIZE
    size = s.IOSize+rem           #adjust for front bytes
    pages = size//PAGE_SIZE
    if size%PAGE_SIZE: pages += 1
    size = pages*PAGE_SIZE
    s.offset = rem+idxoff
    return s.IOBase-rem,size,rem
    
  def gpiommap(s):
    import os, mmap
    addr,size,off=s.calcoffsize()
    try:
      fd = os.open("/dev/mem", os.O_RDWR|os.O_SYNC)
      s.mem = mmap.mmap(fd, size, mmap.MAP_SHARED, mmap.PROT_READ|mmap.PROT_WRITE, offset=addr)
      print("addr {:x}: size {:x} len {:x}".format(addr,size,len(s.mem)))
      return s.mem
    except OSError as e:
      print("addr {:x}: size {:x} mmap failed {}".format(addr,size,e))
      
  def _read(s,off=0x10):
    from struct import unpack
    off+=s.offset
    val=unpack("L",s.mem[off:off+4])[0]
#    print("addr {:x}: res {:x}".format(off,val))
    return val
    
  def _write(s,off,val,mask):
    from struct import pack,unpack
    off+=s.offset
    reg=unpack("L",s.mem[off:off+4])[0]
    nreg=(reg&~mask)|val
    s.mem[off:off+4]=pack("L",nreg)
    print("addr {:x}: set {:x} old {:x} val {:x} mask {:x}".format(off,nreg,reg,val,mask))

  def _writeatomic(s,off,val,mask):
    """use a c extension to avoid problems with python threading - i.e. this way should be 
python threadsave"""
    off+=s.offset
    res=s._frob(val,mask,off,s.mem)
#    print("addr {:x}: set {:x} val {:x} mask {:x}".format(off,res,val,mask))
