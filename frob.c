/* (C) Hermann Lauer 2006,2017
 * may be distributed under the GPL V3 or later
 */

#include <Python.h>

static PyObject *
frob(PyObject *self, PyObject *pyargs)
{
    Py_buffer buf;
    unsigned int *data;
    unsigned int val,mask,offset;

    if (!PyArg_ParseTuple(pyargs, "iiiy*", &val, &mask, &offset, &buf))
        return NULL;
    data=(unsigned int *)(buf.buf+offset);
    *data=(*data&~mask)|val;
    return Py_BuildValue("i", *data);
}

static PyMethodDef atomicfrob_methods[] = {
    {"frob",  frob, METH_VARARGS,
     "set bits in mask to val"},
   {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef atomicfrob = {
    PyModuleDef_HEAD_INIT,
    "atomicfrob",
    NULL,
    -1,
    atomicfrob_methods
};

PyMODINIT_FUNC
PyInit_atomicfrob(void)
{
  return PyModule_Create(&atomicfrob);
}
