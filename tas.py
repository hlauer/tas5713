#!/usr/bin/python3
#
# (C) Hermann Lauer 2006-2017
# can be distributed under the GPL V3 or later

from smbus import SMBus

class smbus(object):
  """define a smbus interface.
This is now mainly a wrapper around c module python-smbus: smbus.SMBus,
which cannot be subclassed.
There is git://github.com/bivab/smbus-cffi.git, but that is not python threadsave"""

  def __init__(s,f=0):
    s.bus=SMBus(f)
    s.errcnt=0
    s.Read=      s.bus.read_byte      #redefine for direct access      
    s.Read_byte= s.bus.read_byte_data
    s.Read_word= s.bus.read_word_data
    s.Write=     s.bus.write_byte
    s.Write_byte=s.bus.write_byte_data
    s.Write_word=s.bus.write_word_data

  def error(s,cause,name=""):
    """called when an IOError occurs"""
    import syslog

    s.errcnt+=1
    syslog.syslog(syslog.LOG_ERR,"%s: cause(%i)=%s"%(name, s.errcnt, str(cause)))
    if cause.errno not in (5,6,16,19,35,110): raise cause

#EIO	5	Input/output error(Failed transaction or Bus collision)
#ENXIO	6	No such device or address(No answer from smbus device)
#EBUSY	16	Device or resource busy(Reset timed out)
#ENODEV	19	No such device: Old patched No answer from smbus device
#EDEADLK 35	Old patched buscollision
#ETIMEDOUT 110	Transaction timed out 

	
#########################################
# clients

class MaxPort(object):
  """access to the 16 io port bits of a maxim 7311"""

  def __init__(s,smbus,addr=0x20):
    s.smbus=smbus
    s.addr=addr
#	s.resync()			#this must be done explicit (parbus hack) 

  def resync(s):
    s.dir=s.smbus.Read_word(s.addr,6)	#note state of direction
    s.out=s.smbus.Read_word(s.addr,2)	#note state of output

  def set_dir(s,dir):
    if dir==s.dir: return
    s.smbus.Write_word(s.addr,6,dir)
    s.dir=dir

  def write(s,out):
    """write output bits, but avoid unessesary i2c traffic"""
#        print("{0:04x}".format(out))
    todo=s.out^out
    if todo&0xff:
      if todo&0xff00: s.smbus.Write_word(s.addr,2,out)
      else: s.smbus.Write_byte(s.addr,2,out&0xff)
    else:
      if todo&0xff00: s.smbus.Write_byte(s.addr,3,out>>8)
    s.out=out

  def write_direct(s,out):
    """write output bits direct, so we are shure we wrote it"""

    s.smbus.Write_word(s.addr,2,out)
    s.out=out

  def read(s):
    """the low 8 bits are alway output (with my setup)"""
    r=s.smbus.Read_byte(a.addr,1)
    r=r<<8|s.out&0xff;
    return r

  def writebit(s,bit,pos):
    """write output bit at given position"""
    bitm=1<<pos
    out=(s.out&~bitm)|(bit<<pos)
    s.write(out)

  def getbit(s,pos):
    """get bit from output at given position"""
    bitm=1<<pos
    return(s.out&bitm)>>pos

  def shiftmask(s,pos):
    """shift and mask from position"""
    return (pos,1<<pos)

#####################################################
class PCFPort(object):
  """access to the 8 io port bits of a PCF8574
/* Portbelegung PCF8574 when used for LCD                         */
/* P.0= RS        D1                                              */
/* P.1= R/W       D2                                              */
/* P.2= E         D3                                              */
/* P.3= LCD-Light D0                                              */
/* P.4= D4                                                        */
/* P.5= D5                                                        */
/* P.6= D6                                                        */
/* P.7= D7                                                        */
/******************************************************************/
/* Adressen: (nur gerade Werte !)                                 */
/*  0x40-0x4E : PCF8574   Format 0b 0 1 0 0 A2 A1 A0 0            */
"""

  def __init__(s,smbus,addr=0x24):	#Beschriftung A0-A2 ist invertiert

    s.smbus=smbus
    s.addr=addr

  def read(s):
    return s.smbus.Read(s.addr)

  def write(s,val):
    return s.smbus.Write(s.addr,val)

###################################################
class PWMPort(object):
  """PCA9531 contains two timers for PWM"""

  def __init__(s,smbus,addr=0x60):
    s.smbus=smbus
    s.addr=addr
    s.timer=[]
    s.timer.append(smbus.Read_word(s.addr,0x11))	#Auto increment
    print(hex(s.timer[-1]))
    s.timer.append(smbus.Read_word(s.addr,0x13))	#Auto increment
    print(hex(s.timer[-1]))
    s.out=s.smbus.Read_word(s.addr,0x15)	#output selector
    print(hex(s.out))

  def write(s,out):
    """write output bits, but avoid unessesary i2c traffic"""
    todo=s.out^out
    if todo&0xff:
      if todo&0xff00: s.smbus.Write_word(s.addr,0x15,out)
      else: s.smbus.Write_byte(s.addr,0x05,out&0xff)
    else:
      if todo&0xff00: s.smbus.Write_byte(s.addr,0x06,out>>8)
    s.out=out

  def set_freq(s,freq,timer=0):
    """0.006(0)-1.69(255) sekunden Periode, 152-0.6 Hz"""

    per=int(152.0/freq-1+0.5)	#round up
    if per <0 or freq>152: raise ValueError
    print(per)
    s.smbus.Write_byte(s.addr,0x01+2*timer,per)

  def set_duty(s,duty,timer=0):
    """0.006(0)-1.69(255) sekunden Periode, 152-0.6 Hz"""

    reg=int(duty*256+0.5)	#round up
    if reg<0: reg=0		#clip values
    if reg>255: reg=255
    if s.timer[timer]>>8 != reg:
      s.smbus.Write_byte(s.addr,0x02+2*timer,reg)
      s.timer[timer]=s.timer[timer]&0xff | (reg<<8)
    return reg/256.0

  def writebit(s,bit,pos):
    """write output bit at given position
    0,1: off,on
    2,3: blink with timer0, timer1"""

    bitm=3<<(pos*2)
    out=(s.out&~bitm)|(bit<<(pos*2))
    s.write(out)

  def getbit(s,pos):
    """get bit from output at given position"""
    bitm=3<<(pos*2)
    return(s.out&bitm)>>(pos*2)

  def shiftmask(s,pos):
    """shift and mask from position"""
    return (pos*2,3<<(pos*2))

##################################################
class PortBit(object):
  """use a bit from a port for writing
remembers a inversion flag and the position"""

  def __init__(s,port,pos,inv=0):
    s.port=port
    s.pos=pos
    s.inv=inv

  def write(s,bit):
    """write output bit at given position"""
    s.port.writebit(bit^s.inv,s.pos)

  def state(s):
    """Return state of output bit"""
    return s.port.getbit(s.pos) ^ s.inv

##############################################
class TAS5713(object):
  """AMP+ Soundchip"""

  def __init__(s,smbus,addr=0x1b):
    s.smbus=smbus
    s.addr=addr
    ret=s.smbus.Read_word(s.addr,0)
    print(hex(ret))

  def readN(s,sub,cnt=4):
    rd=s.smbus.bus.read_i2c_block_data(s.addr,sub,cnt)
    res=0
    for i in rd:
      res=res<<8 | i
    return res

  def writeN(s,sub,val,cnt=4):
    if type(val)==int:
      bytes=[]
      for i in range(cnt):
        bytes.append(val&0xff)
        val>>=8 
      bytes.reverse()
      print(bytes)
    else:
      bytes=val
    
    s.smbus.bus.write_i2c_block_data(s.addr,sub,bytes)

  def writeb(s,sub,val):
    s.smbus.Write_byte(s.addr,sub,val)

  def info(s):
    read=s.smbus.Read
    
    clockctl=s.smbus.Read_byte(s.addr,0)
    devid=read(s.addr)
    stat=s.status()
    sysctl1=read(s.addr)
    serint=read(s.addr)
    sysctl2=read(s.addr)
    sofmut=read(s.addr)
    mvol=read(s.addr)
    chan1=read(s.addr)
    chan2=read(s.addr)
    chan3=read(s.addr)
    volconf=s.smbus.Read_byte(s.addr,0xe)
    bknd_err=s.smbus.Read_byte(s.addr,0x1c)
    inmult=s.readN(0x20)
    ch4source=s.readN(0x21)
    pwmomux=s.readN(0x25)
    drcctl=s.readN(0x46)
    banksweq=s.readN(0x50)
    return "clockctl {:x} devid {:x} stat {:x} sysctl1 {:x} serint {:x} sysctl2 {:x}\n"\
        "softmut {:x} mvol {:x} chan1 {:x} chan2 {:x} chan3 {:x} volconf {:x} BKND_ERR {:x}\n"\
          "inmult {:x} ch4source {:x} pwmomux {:x} drcctl {:x} banksweq {:x}".format(
          clockctl, devid, stat, sysctl1, serint, sysctl2,
            sofmut, mvol, chan1, chan2, chan3, volconf,
            bknd_err, inmult, ch4source, pwmomux, drcctl, banksweq)

  def seteq(s,on=0):
    if on==0:
      s.writeN(0x50,0x90)
    
  def status(s):
#    s.smbus.Write(s.addr,2)
    stat=s.smbus.Read_byte(s.addr,2)
    print(hex(stat))
    return stat

  def get323(s,val,shift=0):
    val>>=shift*32
    val&=0x3ffffff
    if val&0x2000000:
      val=-((val^0x3ffffff)+1)
    return val/(1<<23)

  def biquad(s,off):
    c=s.readN(off,20)
    b0=s.get323(c,4)
    b1=s.get323(c,3)
    b2=s.get323(c,2)
    a1=s.get323(c,1)
    a2=s.get323(c)
    print("b0={} b1={} b2={} a1={} a2={}".format(b0,b1,b2,a1,a2))

  def calc323(s,val):
    if val>=0: return int(val*(1<<23))&0x1ffffff
    v=int(-val*(1<<23))&0x3ffffff
    return ((v-1)^0x3ffffff)|0x2000000

  def setbiquad(s,off,coeffs):
    b0,b1,b2,a1,a2=coeffs

    def c4(val):
      val=s.calc323(val)
      print(hex(val))
      res=[]
      for i in range(4):
        res.append(val&0xff)
        val>>=8
      res.reverse()
      return res

    all=c4(b0)+c4(b1)+c4(b2)+c4(a1)+c4(a2)
    print(all)
    s.writeN(off,all)

  def infovol(s):
    ch1_right=s.readN(0x72)
    ch1_left=s.readN(0x73)
    ch2_left=s.readN(0x76)
    ch2_right=s.readN(0x77)
    return "ch1_right {} ch1_left {} ch2_left {} ch2_right {}".format(
          s.get323(ch1_right), s.get323(ch1_left), s.get323(ch2_left), s.get323(ch2_right))
    
  def setup(s):
    s.writeN(0x25,0x1021345)    #ch1<>ch2: 0x1130245
    s.setModulation()
    on=s.calc323(1)
    s.writeN(0x72,0)
    s.writeN(0x73,on)
    s.writeN(0x76,0)
    s.writeN(0x77,on)

  def setModulation(s,bd=True):
    writeb=s.smbus.Write_byte
    if bd:
#      s.write_byte(0x10,2)     #default
      s.writeb(0x11,0xb8)
      s.writeb(0x12,0x60)
      s.writeb(0x13,0xa0)
      s.writeb(0x14,0x48)
      s.writeN(0x20,0x897772)   # 0x987772 to exchange L<>R
    else:
      s.writeb(0xac,0xb8)
      s.writeb(0x54,0x60)
      s.writeb(0xac,0xa0)
      s.writeb(0x54,0x48)
      s.writeN(0x20,0x017772)
    
  def setlowpass(s,off):
    s.setbiquad(off,
                1.000749011855770,
                -1.974713476217000,
                0.974143683120370,
                1.974723221152960,
                -0.974882950040180)
#             1.000150538944590,
#            -1.994918633558090,
#             0.994775336200099,
#             1.994919027315960,
#            -0.994925481386818)

fb=""" low pass
100Hz
a0	1
a1	1.987201380239890
a2	-0.987362118444486
	
b0	1.000379324037070
b1	-1.987191573726250
b2	0.986992600921048
20Hz
a0	1
a1	1.994919027315960
a2	-0.994925481386818
	
b0	1.000150538944590
b1	-1.994918633558090
b2	0.994775336200099
end"""

class fb(dict):
  def __init__(s,fb=fb):
    key=None
    v={}
    for l in fb.splitlines():
      sl=l.split(None,1)
      if len(sl)==2:
        if sl[1].startswith('= '): sl[1]=sl[1][2:]       #allow "= val" syntax
        try:
          v[sl[0]]=float(sl[1])
        except: pass
        continue
      if len(sl):
        if key:
          s[key]=(v['b0'],v['b1'],v['b2'],v['a1'],v['a2'])
        v={}
        key=sl[0]

filter=fb(open("biquad.filter").read())

##################################
#main follows

if __name__ == '__main__':
  try:
    import sys
    if sys.implementation._multiarch.startswith('arm'):
      sm=smbus(1)
      addr=0x1b
      print("arm: {:x}".format(addr))
    else: raise AttributeError
  except AttributeError:
    sm=smbus()
    addr=0x20

  t=TAS5713(sm)
  print(t.info())
  print(t.infovol())
  t.setup()
  t.setbiquad(0x29,filter['Lt70/20Hz.5'])
  t.setbiquad(0x2a,filter['default'])
#fail  t.setbiquad(0x2a,filter['peak17k1.584-5'])
#  t.setbiquad(0x2a,filter['highshelf13k-4'])
#  t.setbiquad(0x2a,filter['lowone15k'])
  t.setbiquad(0x30,filter['Lt70/20Hz.5'])
  t.setbiquad(0x31,filter['default'])
#  t.setbiquad(0x31,filter['peak17k1.584-5'])
#  t.setbiquad(0x31,filter['highshelf13k-4'])
#  t.setbiquad(0x31,filter['lowone15k'])
#  t.setbiquad(0x29,filter['400Hz.5'])
#  t.setbiquad(0x30,filter['400Hz.5'])
  
#/usr/local/sbin# i2cdetect -l

#look for i2c clients:
#i2cdetect -a 0
