.. -*- coding: utf-8; mode: rst -*-
Note
####

please look at https://foss.heptapod.net/hlauer/tas5713 for a recent version!


#######################################
HOWTO USE HIFIBERRY AMP+ WITH BANANAPRO
#######################################

**Copyright** |copy| 2017 : Hermann Lauer

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.1 or
any later version published by the Free Software Foundation.

Hardware Setup
##############
You need a custom cable connection between the BananaPro and the
HifiBerry AMP+. Directly connecting (as designed for the RasberryPi)
will not work and is DANGEROUS, as BananaPro didn't allow 5V power
input over the GPIO header (40 pin). As the I2S pins (4 at bananapro,
rasberry lacks MCLK) are at another position, thats probably the
easiest solution.

HiFiBerry	BananaPro GPIO# 	Cablecolor (in my setup)
3  SDA		3      I2C_SDA		blue
5  SCL		5      I2C_SCL		brown
12 MCLK,SCLK	36(B6) I2S0_BCLK	red
35 LRCLK	39(B7) I2S0_LRCK	orange
40 SDIN		40(B8) I2S0_DO0		yellow
4+39 GND	34     GND		black


Custom Kernel (Modules)
#######################
The patches on "https://bitbucket.org/hlauer/kernel/src", subdirectory
"patches" are developed for the 4.9.X (tested for X<=21) kernel which is
at the moment the long term kernel supported for Debian stretch. It
works also with bananian 16.04 (based on Debian jessie).

To build the kernel (module), prepare your BananaPro with:
# apt-get install build-essential
# apt-get build-dep linux

download the 4.9.X kernel, unpack it and change
into the kernel directory, then:

$ cp /boot/config-4.9.* .config
edit .config to set CONFIG_SYSTEM_TRUSTED_KEYS="". This is new in
stretch to clear out the maintainers key.

$ patch -p1 <[patchname]
with [patchname] beeing the first patch from the series file. Repeat
until "sun4i-i2s.patchl", the next one is optional for debugging.

If all went ok:
$ make oldconfig
answer the questions for the hifiberry and tas5713 with 'm', then:
$ make -j2 bindeb-pkg
wait approx. half of a day until the kernel build finishes. Then as root
install the kernel debs from the parent directory:
# dpkg -i linux-image...deb linux-firmware...deb

blacklist the new modules in the new file /etc/modprobe.d/hifiberry.conf:

 blacklist snd_soc_hifiberry_amp
 blacklist snd_soc_tas5713

reboot.

Using the modules
#################
Download https://bitbucket.org/hlauer/tas5713/get/default.tar.bz2,
unpack and then:

# cd tas5713
# python3 tas.py
# python3 bananaio.py
# modprobe snd_soc_tas5713
# modprobe snd_soc_hifiberry_amp

Alsamixer then should show the single "master" and a stereo "channel"
control. Please lower master below 40% to avoid damage to your
speakers (and your ears) before playing anything !

Caveeats and improvements
#########################
The modules are adapted from rasberry and are seen of alpha quality. Trying
to unload them usually ends with a kernel oops, after which you have
to reboot. In kernels newer than 4.9.21 the i2s driver code has changed.

Sampling speed can only be 44.1kHz and 48kHz - due to the missing MCLK
input on the HiFiBerry board.

bananaio.py is needed to set up the I2S function on the relevant pins.
An improved version would do this with the kernel device tree.

tas.py prepares the tas5713 chip and loads a BiQuad filter improving
the sound of my speakers. Hunt for "All-digital-coefs v1.2.xls" on the
web if you like to design your own filter.

It's not clear if left and right are transferred correctly over the I2S.
Both arm I2S and tas5713 allow to exchange the channels. Somebody
with a scope should test that on the I2S.

At least with pulseaudio there seem to be idle scheduling problems -
as a workaround, on an unloaded system a busy loop like Pythons
"while True: pass" helps. Or try "# CONFIG_CPU_IDLE is not set".
