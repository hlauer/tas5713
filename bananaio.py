#!/usr/bin/python3
#
# (C) Hermann Lauer 20016-2017
# can be distributed under the GPL V3 or later

# bananaPi/Pro layout
#	-1, -1, //1, 2
#	53, -1, //3, 4  I2C-SDA
#	52, -1, //5, 6  I2C-SCL
#	259, 224, //7, 8   UART-TX
#	-1, 225, //9, 10   UART-RX
#	275, 226, //11, 12
#	274, -1, //13, 14
#	273, 244, //15, 16
#	-1, 245, //17, 18
#	268, -1, //19, 20
#	269, 272, //21, 22
#	267, 266, //23, 24
#	-1, 270, //25, 26
#
#       272-275 or 266-270
#	
#	257, 256, //27, 28
#	35, -1, //29, 30
#	277, 276, //31, 32
#	45, -1, //33, 34
#	39, 38, //35, 36        LRCLK,BCLK
#	37, 44, //37, 38
#	-1, 40, //39, 40        DO0
GPIO_BASE_BP    = 0x01C20000
SUNXI_GPIO_BASE = 0x01C20800
BLOCK_SIZE = 4*1024

import mmapio

class gpio(mmapio.mmapio):
  IOBase=0x01C20800
  Size=0x400
  Count=1

  offset=SUNXI_GPIO_BASE-GPIO_BASE_BP   #calc offset in BLOCK aligned mmap

  def __init__(s,idx=0,needatomic=False):
        """if needatomic is true, loading the c ext for frobbing bits must succeed"""

        s.IOSize=s.Size*s.Count
        super().__init__(needatomic)
        s.calcoffsize(s.Size*idx)       #set internal offset to index
    
  def write(s,pin,val,mask):
    """write a bank containig pins as 32 bit - mask must be inside"""
    bank,idx=s.calcphy(pin)
#    print(bank,idx)
    return s._write(bank,val<<idx,mask<<idx)

  def write(s,pin,val,mask):
    """write a bank containig pins as 32 bit - mask must be inside"""
    bank,idx=s.calcphy(pin)
#    print(bank,idx)
    return s._write(bank,val<<idx,mask<<idx)

  def read(s,pin):
    """read bank containig pin as 32 bit"""
    bank,idx=s.calcphy(pin)
    return s._read(bank+0x10)

  def readbit(s,pin):
    """read pin"""
    bank,idx=s.calcphy(pin)
    return (s._read(bank+0x10)>>idx)&1
  
  def calcphy(s,pin):
    """return bank offset (*0x24), index"""
    bank = pin >> 5
    index = pin - (bank << 5)
    return bank * 36,index

  def calcselmask(s,pin):
    """for configure registers:
return addr offset, idx shift, mask"""

    bank,pin=s.calcphy(pin)
    reg = pin>>3
    idx = pin&7
    return reg*4+bank, idx*4, 7<<(idx*4)

  def getDir(s,pin):
    reg,idx,mask=s.calcselmask(pin)
    print("reg {:x} idx {:x} mask {:x}".format(reg,idx,mask))
    return (s._read(reg)&mask)>>idx

  def setDir(s,pin,dir):
    reg,idx,mask=s.calcselmask(pin)
#    print("reg {:x} idx {:x} mask {:x}".format(reg,idx,mask))
    s._write(reg,dir<<idx,mask)

  def calcdrvmask(s,pin):
    """for current driver registers:
return addr offset, idx shift, mask"""

    bank,pin=s.calcphy(pin)
    reg = pin>>4
    idx = pin&0xf
    return reg*4+bank, idx*2, 3<<(idx*2)

  def getDrv(s,pin):
    reg,idx,mask=s.calcdrvmask(pin)
    print("reg {:x} idx {:x} mask {:x}".format(reg+0x14,idx,mask))
    return (s._read(reg+0x14)&mask)>>idx

  def setDrv(s,pin,drv):
    reg,idx,mask=s.calcdrvmask(pin)
    s._write(reg+0x14,drv<<idx,mask)

  def getPull(s,pin):
    reg,idx,mask=s.calcdrvmask(pin)
    print("reg {:x} idx {:x} mask {:x}".format(reg+0x1c,idx,mask))
    return (s._read(reg+0x1c)&mask)>>idx

  def setPull(s,pin,pull):
    reg,idx,mask=s.calcdrvmask(pin)
    s._write(reg+0x1c,pull<<idx,mask)

  def showdir(s,idx=0):
    r=s._read(idx)
    for i in range(32//3):
      print("{}\t{}".format(i,r&7))
      r>>=3

class CCU(mmapio.mmapio):
    IOBase=0x01C20000
    Size=0x200
    Count=1
    
    def __init__(s,idx=0,needatomic=False):
        """if needatomic is true, loading the c ext for frobbing bits must succeed"""
        s.IOSize=s.Size*s.Count
        super().__init__(needatomic)
        s.calcoffsize(s.Size*idx)       #set internal offset to index

    def info(s):
        """print interesting registers"""
        print("PLL2_CFG {:x}, PLL2_TUN {:x}, IIS0_CLK {:x}, IIS1_CLK {:x}".format(
          s._read(8), s._read(0xc), s._read(0x8b), s._read(0xd8)), end=', ')
        print("IIS2_CLK {:x}".format(
          s._read(0xdc)))
      
#########################################

if __name__ == '__main__':
  g=gpio()
#g.getDir(38)  #I2S_BCLK
#g.getDir(39)  #I2S_LRCLK
#g.getDir(40)  #I2S_DO0
  g.setDir(38,2)  #I2S_BCLK
  g.setDir(39,2)  #I2S_LRCLK
  g.setDir(40,2)  #I2S_DO0

  ccu=CCU()
  ccu.info()
  tun=ccu._read(0xc)
  if tun:
    print("PLL2_TUN {:x}, setting to 0 !".format(tun))
    ccu._write(0xc,0,0xffffffff)
