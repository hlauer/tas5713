# (C) Hermann Lauer 20016-2017
# can be distributed under the GPL V3 or later

atomicfrob.so: atomicfrob.o
	ld -shared $< -o atomicfrob.so

#	ld -shared -export-dynamic $< -o i2cmodule.so

atomicfrob.o: frob.c
	cc -I /usr/include/python3.5 -c -fPIC $< -o $@
#	cc -I /usr/include/python3.4 -c -fPIC $< -o $@

install:
	cp tas.py biquad.filter /usr/local/src/tas5713
